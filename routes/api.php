<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('one')->group(function () {
    Route::get('eps', [\App\Http\Controllers\PointOneController::class, 'eps']);
    Route::get('symptoms', [\App\Http\Controllers\PointOneController::class, 'symptoms']);
    Route::get('users', [\App\Http\Controllers\PointOneController::class, 'users']);
    Route::post('punto-1', [\App\Http\Controllers\PointOneController::class, 'pointOne']);
});

Route::prefix('two')->group(function () {
    Route::get('genders', [\App\Http\Controllers\PointTwoController::class, 'gender']);
    Route::get('users', [\App\Http\Controllers\PointOneController::class, 'users']);
    Route::post('punto-2', [\App\Http\Controllers\PointTwoController::class, 'pointTwo']);
});

