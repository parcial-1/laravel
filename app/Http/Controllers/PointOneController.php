<?php

namespace App\Http\Controllers;

use App\Models\Eps;
use App\Models\Symptom;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class PointOneController extends Controller
{
    public function pointOne(Request $request)
    {
        $validatedData = $this->validate($request, [
            'name' => 'required',
            'lastname' => 'required',
            'identification_number' => 'required',
            'eps_id' => 'required|exists:eps,id',
            'symptom_id' => 'required|exists:symptoms,id',
            'blood_glucose_results' => 'required_if:symptom_id,1|required_if:symptom_id,2|required_if:symptom_id,3',
        ]);

        $bloodGlucoseResults = $validatedData['blood_glucose_results'];

        if ($bloodGlucoseResults >= 7 && $bloodGlucoseResults <= 13.8) {
            $message = 'Indicar glucemia en ayunas y TGP en pacientes sin diagnóstico. - Si deshidratación, rehidratación oral o EV según las demandas. - Reevaluar conducta terapéutica en diabéticos y cumplimiento de los pilares. - Reevaluar dosis de hipoglucemiantes';
        } else if ($bloodGlucoseResults >= 13.8 && $bloodGlucoseResults <= 33) {
            $message = 'Coordinar traslado y comenzar tratamiento. - Hidratación con Solución salina 40 ml/Kg en las primeras 4 horas. 1-2 L la primera hora. - Administrar potasio al restituirse la diuresis o signos de hipopotasemia (depresión del ST, Onda U ≤ 1mv, ondas U≤ T). - Evitar insulina hasta desaparecer signos de hipopotasemia. - Administrar insulina simple 0,1 U/kg EV después de hidratar';
        } else if ($bloodGlucoseResults > 33) {
            $message = 'Coordinar traslado y comenzar tratamiento. - Hidratación con Solución Salina 10-15 ml/Kg/h hasta conseguir estabilidad hemodinámica. - Administrar potasio al restituirse la diuresis o signos de hipopotasemia (depresión del ST, Onda U ≤ 1mv, ondas U≤ T). ';
        } else {
            $message = 'no cumple ninguna condicion';
        }

        User::create($validatedData);

        return response()->json([
            'data' => compact('message')
        ]);
    }

    public function eps()
    {
        return response()->json([
            'data' => [
                'eps' => Eps::all()
            ]
        ]);
    }

    public function symptoms()
    {
        $symptoms = Symptom::all();
        $symptoms->add([
            'id' => 4,
            'name' => 'ninguno',
        ]);
        return response()->json([
            'data' => [
                'symptoms' => $symptoms
            ]
        ]);
    }

    public function users()
    {
        return response()->json([
            'data' => [
                'users' => User::with('eps','symptom')->get()
            ]
        ]);
    }
}
