<?php

namespace App\Http\Controllers;

use App\Models\Gender;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PointTwoController extends Controller
{
    public function gender(): JsonResponse
    {
        return response()->json([
            'data' => [
                'gender' => Gender::all()
            ]
        ]);
    }

    public function pointTwo(Request $request): JsonResponse
    {
        $validatedData = $this->validate($request, [
            'name' => 'required',
            'lastname' => 'required',
            'unit_time' => 'required|in:1,2',
            'hemoglobin_level' => 'required|numeric',
            'email' => 'required|email|unique:users',
            'age' => 'required|integer',
            'gender_id' => 'required|exists:genders,id',
        ]);

        if ($validatedData['unit_time'] === 2) {
            $months = $validatedData['age'] * 12;
        } else {
            $months = $validatedData['age'];
        }

        $hemoglobinLevel = $validatedData['hemoglobin_level'];
        $anemia = false;
        if (($months >= 0 && $months <= 1) && ($hemoglobinLevel >= 13 && $hemoglobinLevel <= 26)) {
            $anemia = true;
        } else if (($months > 1 && $months <= 6) && ($hemoglobinLevel >= 10 && $hemoglobinLevel <= 18)) {
            $anemia = true;
        } else if (($months > 6 && $months <= 12) && ($hemoglobinLevel >= 11 && $hemoglobinLevel <= 15)) {
            $anemia = true;
        } else if (($months > 12 && $months <= 59.999999) && ($hemoglobinLevel >= 11.5 && $hemoglobinLevel <= 15)) {
            $anemia = true;
        } else if (($months > 59.999999 && $months <= 120) && ($hemoglobinLevel >= 12.6 && $hemoglobinLevel <= 15)) {
            $anemia = true;
        } else if ($months > 180) {
            if ($validatedData['gender_id'] === 2 && ($hemoglobinLevel >= 12 && $hemoglobinLevel <= 16)) {
                $anemia = true;
            } else if ($validatedData['gender_id'] === 1 && ($hemoglobinLevel >= 14 && $hemoglobinLevel <= 18)) {
                $anemia = true;
            }
        }

        $message = 'No Tienes anemia :D';

        if (!$anemia)
            $message = 'Tienes anemia!!!';

        return response()->json([
            'data' => [
                'message' => $message
            ]
        ]);
    }
}
